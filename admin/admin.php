<?php require_once('config/database.php')?>
<?php require_once('config/auth.php') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> ADMIN | Dashboard</title>
    <?php require_once('layout/css.php') ?>
    
</head>
<body>
    
    <div class="wrapper">
        <!-- sidebar -->
        <?php
            require_once('layout/sidebar.php');
        ?>
        <!-- end sidebar -->

        <div class="contents">
            
            <!-- header -->
            <?php require_once('layout/header.php') ?>
            <!-- end header -->

            <!-- content -->
            <div class="wrap">
                <div class="main">
                    <div class="name">Admin</div>
                    <a href="new-admin.php" class="button">Add New Admin</a>
                </div>
                    <table border="1" style="width:100%">
                        <tr>
                            <th>NO</th>
                            <th>NAME</th>
                            <th>USERNAME</th>
                            <th>PASSWORD</th>
                            <th>OPSI</th>
                        </tr>

                        <?php

                            if(isset($_GET['delete'])){
                                $delete = $_GET['delete'];
                                $sql = "DELETE FROM login WHERE id = '$delete' ";
                                mysqli_query($connect, $sql);
                                
                                header('location:admin.php?message=Data Berhasil diHapus');

                            }

                            $user = mysqli_query($connect, "SELECT * FROM login");
                            if(mysqli_num_rows($user) > 0){
                                $no = 1;
                                while($data = mysqli_fetch_assoc($user)){
                                    ?>
                                    
                                    <tr>
                                        <td><?=$no++?></td>
                                        <td><?=$data['name']?></td>
                                        <td><?=$data['username']?></td>
                                        <td><?=$data['password']?></td>
                                        <td>
                                            <a href="edit-admin.php?id=<?= $data['id']?>" class="edit"><i class="fa fa-edit"></i></a>
                                            <a href="?delete=<?= $data['id']; ?>" class="del" onclick="return confirm('Anda yakin akan menghapus data user ini?')"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                        ?>

                    </table>
            </div>
            <!-- end contents -->


        </div>
    </div>


</body>
</html>