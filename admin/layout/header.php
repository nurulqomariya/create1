<div class="header">
    <div class="head-left">
        <i class="fa fa-navicon"></i>
    </div>
    <div class="head-right">
        <div class="main">
            <div class="toggle">  Administrator <i class="fa fa-user"></i>  </div>
            <ul>
                <li><a href=""> <i class="fa fa-user"></i> Profile</a></li>
                <li><a href="logout.php"> <i class="fa fa-sign-out"></i> Logout</a></li>
            </ul>
        </div>
    </div>
</div>