<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> ADMIN | Dashboard</title>
    <?php require_once('layout/css.php'); ?>
</head>
<body>
    
    <div class="wrapper">
       <!-- sidebar -->
       <?php
            include('layout/sidebar.php');
        ?>
        <!-- end sidebar -->

        <div class="contents">
            
             <!-- header -->
             <?php require_once('layout/header.php'); ?>
            <!-- end header -->


            

        </div>
    </div>

</body>
</html>