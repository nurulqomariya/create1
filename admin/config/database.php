<?php
    ini_set('date.timezone', 'Asia/Jakarta');

    $hostname   = 'localhost';
    $username   = 'root';
    $password   = '';
    $dbname     = 'admin';

    $connect    = mysqli_connect($hostname, $username, $password, $dbname);
    if(!$connect){
        echo "<h1>GAGAL KONEKSI KE DATABASE</h1>";
        exit;
    }
?>