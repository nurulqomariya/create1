<?php require_once('config/database.php')?>
<?php require_once('config/auth.php') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> ADMIN | Dashboard</title>
    <?php require_once('layout/css.php') ?>
</head>
<body>
    
    <div class="wrapper">
        
        <!-- sidebar -->
        <?php
            require_once('layout/sidebar.php');
        ?>
        <!-- end sidebar -->

        <div class="contents">
            
            <!-- header -->
            <?php require_once('layout/header.php') ?>
            <!-- end header -->
            
            <?php
                if (isset($_GET['id'])){
                    $id = $_GET['id'];

                    $sql = "SELECT * FROM login WHERE id = '$id'";
                    $query = mysqli_query($connect, $sql);

                    if(mysqli_num_rows($query) > 0){
                        $data = mysqli_fetch_assoc($query);
                    }else{
                        header('location:admin.php');
                    }

                }else{
                    header('location:admin.php');
                }

                if(isset($_POST['create'])){
                    $n = $_POST['nama'];
                    $us = $_POST['username'];
                    $ps = $_POST['password'];

                    $sql = "UPDATE login SET name='$n', username='$us', password='$ps' WHERE id='$id' ";
                    mysqli_query($connect, $sql);
                    header('location:admin.php');

                }
            ?>

            <!-- content -->
            <div class="wrap">
                <div class="main">
                    <div class="name">Edit</div>
                    <a href="admin.php" class="tombol">Back</a>
                </div>
                
                <form action="admin.php" method="post">
                    Nama     : <br> <input type="text" name="nama" value="<?=$data['name']?>" placeholder="Masukkan Nama Anda"><br>
                    Username : <br> <input type="text" name="username" value="<?=$data['username']?>" placeholder="Masukkan Username Anda"><br>
                    Password : <br> <input type="text" name="password" value="<?=$data['password']?>" placeholder="Masukkan Password Anda"><br>
                    <br> <button type="submit" name="create" class="tombol">Edit</button>
                </form>

            </div>
        </div>
    </div>

</body>
</html>