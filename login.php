<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="assets/css/login.css">
</head>
<body>
    <div id="card">
        <div id="card-content">
            <div id="card-tittle">
                <h2>LOGIN</h2>
                <div class="underline-title">
                </div>
            </div>

            <?php 
                require_once('admin/config/database.php');
                $err = " ";
                if(isset($_POST['signin'])){
                    $u = $_POST['name'];
                    $p = $_POST['pass'];

                    $sql = "SELECT * FROM login WHERE username='$u' AND password='$p'";
                    $check = mysqli_query($connect, $sql);

                    if(mysqli_num_rows($check) > 0){
                        $data = mysqli_fetch_assoc($check);
                        session_start();
                        $_SESSION['auth'] = $data;

                        header('location:admin/index.php');
                    }else{
                        $err = 'Username atau Password anda salah';
                    }
                }
            ?>

            <form action="" method="post" class="form">
                <label for="user-name" style="padding-top:13px">&nbsp;Username</label>
                <input type="text" name="name" id="user-email" class="form-content" autocomplete="on" required/>
                <div class="form-border"></div>

                <label for="user-password" style="padding-top:22px">&nbsp;password</label>
                <input type="password" name="pass" id="user-password" class="form-content" required/>
                <div class="form-border"></div>

                <a href="#" class="a"><legend id="forgot-pass">Forgot password?</legend></a>

                <input type="submit" name="signin" value="LOGIN" id="submit-btn" />
                <a href="register.php" id="signup">Don't have account yet?</a>
            </form>
        </div>
    </div>
</body>
</html>